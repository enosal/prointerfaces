package P9_10;

import java.awt.*;

/**
 * Sets x and y position of a rectangle on a coordinate plane and height and width
 * Note: Height and width can be negative integers. A width of -5 from (0,0) would make a side of length 5 connecting to (-5,0)
 * getPerimeter computes and returns the perimeter of the rectangle
 * getArea computes and returns the area of the rectangle
 * Created by Eryka on 10/24/2015.
 */
public class BetterRectangle extends Rectangle {

    /*
        Default constructor
     */
    public BetterRectangle() {
        super.setSize(Math.abs(width), Math.abs(height));
    }

    /*
        Constructor with Rectangle input
     */
    public BetterRectangle(Rectangle r) {
        super.setSize(Math.abs(width), Math.abs(height));
    }

    /*
        Constructor with only width and height
     */
    public BetterRectangle(int width, int height) {
        super.setSize(Math.abs(width), Math.abs(height));
    }

    /*
        Constructor with x,y location and width and height
     */
    public BetterRectangle(int x, int y, int width, int height) {
        super.setLocation(x, y);
        super.setSize(Math.abs(width), Math.abs(height));
    }

    /*
        Constructor with a Point p for location and Dimension d for width and height
     */
    public BetterRectangle(Point p, Dimension d) {
        super.setLocation(p.x, p.y);
        super.setSize(Math.abs(d.width), Math.abs(d.height));
    }

    /*
        Constructor with only a Dimension d for width and height
     */
    public BetterRectangle(Dimension d) {
        super.setSize(Math.abs(d.width), Math.abs(d.height));
    }

    /*
    Constructor with only a Point p for location
    */
    public BetterRectangle(Point p) {
        super.setLocation(p.x, p.y);
    }





    /*
        Computes and returns the perimeter of the rectangle of the given width and height
    */
    public int getPerimeter() {
        return 2 * width + 2 * height;
    }


    /*
        Computes and returns the area of the rectangle of the given width and height
     */
    public int getArea() {
        return width * height;
    }

}
