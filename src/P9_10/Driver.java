package P9_10;

import java.awt.*;

/**
 * Tests BetterRectangle.java
 * Created by Eryka on 10/24/2015.
 */
public class Driver {
    public static void main(String[] args) {
        BetterRectangle rect0 = new BetterRectangle();
        System.out.println("TEST0: No input.");
        System.out.println("Expected Area is 0");
        System.out.println("Actual Area is: " + rect0.getArea());
        System.out.println("Expected Perimeter is 0");
        System.out.println("Actual Perimeter is: " + rect0.getPerimeter());
        System.out.println();

        BetterRectangle rect1 = new BetterRectangle(1, 3, 2, 5);
        System.out.println("TEST1: Constructor with (positive) location, width, height.");
        System.out.println("Expected Area is 10");
        System.out.println("Actual Area is: " + rect1.getArea());
        System.out.println("Expected Perimeter is 14");
        System.out.println("Actual Perimeter is: " + rect1.getPerimeter());
        System.out.println();

        System.out.println("TEST2: Constructor with (negative) location, width, height.");
        BetterRectangle rect2 = new BetterRectangle(-3,-10, 60, 25);
        System.out.println("Expected Area is 1500");
        System.out.println("Area is: " + rect2.getArea());
        System.out.println("Expected Perimeter is 170");
        System.out.println("Perimeter is: " + rect2.getPerimeter());
        System.out.println();

        System.out.println("TEST3: Constructor without a set location and with negative sides");
        BetterRectangle rect3 = new BetterRectangle(-20, 7);
        System.out.println("Expected Area is 140");
        System.out.println("Area is: " + rect3.getArea());
        System.out.println("Expected Perimeter is 54");
        System.out.println("Perimeter is: " + rect3.getPerimeter());
        System.out.println();

        System.out.println("TEST4: Constructor with a Point and Dimension input");
        BetterRectangle rect4 = new BetterRectangle(new Point(3,-4), new Dimension(9, -2));
        System.out.println("Expected Area is 18");
        System.out.println("Area is: " + rect4.getArea());
        System.out.println("Expected Perimeter is 22");
        System.out.println("Perimeter is: " + rect4.getPerimeter());
        System.out.println();

        System.out.println("TEST5: Constructor with only a Point input");
        BetterRectangle rect5 = new BetterRectangle(new Point(5000, 6000));
        System.out.println("Expected Area is 0");
        System.out.println("Area is: " + rect5.getArea());
        System.out.println("Expected Perimeter is 0");
        System.out.println("Perimeter is: " + rect5.getPerimeter());
        System.out.println();

        System.out.println("TEST6: Constructor with only a Dimension input");
        BetterRectangle rect6 = new BetterRectangle(new Dimension(5, 6));
        System.out.println("Expected Area is 30");
        System.out.println("Area is: " + rect6.getArea());
        System.out.println("Expected Perimeter is 22");
        System.out.println("Perimeter is: " + rect6.getPerimeter());
        System.out.println();

    }


}
