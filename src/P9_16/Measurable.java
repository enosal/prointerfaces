package P9_16;

/**
 * Interface for Measurable objects
 * Created by Eryka on 10/24/2015.
 */
public interface Measurable {
    double getMeasure();


    /*
        Returns the maximum of a Measurable array
     */
    static Measurable maximum(Measurable[] objects) {
        double largest = -1;
        int largestI = -1;
        final int size = objects.length;

        /*
            Get total getMeasure
         */
        for (int i = 0; i < size -1; i++) {
            if (objects[i].getMeasure() > largest) {
                largest = objects[i].getMeasure();
                largestI = i;
            }
        }

        if (objects.length == 0) {
            return null;
        }
        else {
            return objects[largestI];
        }


    }

}
