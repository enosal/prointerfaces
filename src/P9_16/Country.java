package P9_16;

/**
 * Creates a Country object with the name and area
 * Created by Eryka on 10/25/2015.
 */
public class Country implements Measurable {
    private String name;
    private double area;

    public Country(String name, double area) {
        this.name = name;
        this.area = area;
    }

    /*
        Returns area
     */
    public double getMeasure() {
        return area;
    }

    /*
        Get Country name
     */
    public String getName() {
        return name;
    }

    /*
        get Country area
     */
    public double getArea() {
        return area;
    }


}
