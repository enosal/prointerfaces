package P9_16;

/**
 * Tests Country and Measurable
 * Created by Eryka on 10/25/2015.
 */
public class Driver {
    public static void  main(String[] args) {
        Measurable[] countries = new Measurable[3];
        countries[0] = new Country("USA", 30000000);
        countries[1] = new Country("Japan", 280000);
        countries[2] = new Country("Poland", 1);

        Measurable[] countries1 = new Measurable[3];

        try {
            System.out.println("TEST1: 3 Countries");
            System.out.println("Largest Country is: " + ((Country) Measurable.maximum(countries)).getName());
            System.out.println();
            System.out.println("TEST2: 0 Countries");
            System.out.println("Largest Country is: " + ((Country) Measurable.maximum(countries1)).getName());
            System.out.println();
        }
        catch (NullPointerException error) {
            System.out.println("NULL returned; No Countries given.");
        }






    }
}
