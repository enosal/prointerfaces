package P9_23;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Prints only objects from the Occurs[] array that occurOn the testDate
 * Created by Eryka on 10/25/2015.
 */
public interface Occurs {
    boolean occursOn(int year, int month, int day);

    static void valid(ArrayList<Occurs> objs, GregorianCalendar testDate) {
        for (Occurs obj : objs) {
            if (obj.occursOn(testDate.get(Calendar.YEAR),testDate.get(Calendar.MONTH), testDate.get(Calendar.DAY_OF_MONTH))){
                System.out.println(obj.toString());
            }

        }
    }



}
