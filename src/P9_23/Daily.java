package P9_23;

import java.util.GregorianCalendar;

/**
 * Create a Daily appointment from a given start date
 * Methods include whether an inputted date occurs on a daily appointment date, and string description of daily appointment
 * Created by Eryka on 10/25/2015.
 */
public class Daily extends Appointment{

    private boolean occurs;

    public Daily(String desc, GregorianCalendar date){
        super(desc, date);
    }

    /*
        Return true if the test date corresponds to a date on or after a daily appointment start date
     */
    public boolean occursOn(int year, int month, int day) {
        GregorianCalendar givenDate = new GregorianCalendar(year, month, day);
        occurs = givenDate.after(super.getDate()) || givenDate.equals(super.getDate());
        return occurs;
    }

    /*
        Returns appointment type (Daily), start date of appointment, description of appointment
     */

    @Override
    public String toString() {
        return "Daily Appointment starting on " + super.dateString() + ": '" + super.getDesc() +"'";
    }


}

