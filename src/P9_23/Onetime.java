package P9_23;

import java.util.GregorianCalendar;

/**
 * Create a onetime appointment from a given date
 * Methods include whether an inputted date occurs on the onetime appointment date, and string description of the onetime appointment
 * Created by Eryka on 10/25/2015.
 */
public class Onetime extends Appointment{

    private boolean occurs;

    public Onetime(String desc, GregorianCalendar date){
        super(desc, date);
    }

    /*
        Return true if the test date corresponds to a onetime appointment date
     */
    public boolean occursOn(int year, int month, int day) {
        GregorianCalendar givenDate = new GregorianCalendar(year, month, day);

        if ((super.getDate()).compareTo(givenDate) == 0) {
            occurs = true;
        }
        else {
            occurs = false;
        }
        return occurs;
    }

    /*
        Returns appointment type (Onetime), start date of appointment, description of appointment
     */
    @Override
    public String toString() {
        return "Onetime Appointment on " + super.dateString() + ": '" + super.getDesc() + "'";
    }

}
