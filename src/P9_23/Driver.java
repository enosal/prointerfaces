package P9_23;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Tests Appointment, Onetime, Monthly, Daily classes, Occurs Interface
 * Add an appointment to an array
 * Created by Eryka on 10/25/2015.
 */
public class Driver {
    public static void main(String[] args) {

        //Array of Appointments
        ArrayList<Occurs> appointments = new ArrayList<>();
        appointments.add(new Monthly("Monthly1: True", new GregorianCalendar(2014, 8, 24)));
        appointments.add(new Monthly("Monthly2: True", new GregorianCalendar(2014, 9, 24)));
        appointments.add(new Monthly("Monthly3: False", new GregorianCalendar(2014, 9, 25)));
        appointments.add(new Monthly("Monthly4: False", new GregorianCalendar(2014, 10, 24)));
        appointments.add(new Daily("Daily1: True", new GregorianCalendar(2014, 9, 23)));
        appointments.add(new Daily("Daily2: True", new GregorianCalendar(2014, 9, 24)));
        appointments.add(new Daily("Daily3: False", new GregorianCalendar(2014, 9, 30)));
        appointments.add(new Onetime("Onetime1: True", new GregorianCalendar(2014, 9, 24)));
        appointments.add(new Onetime("Onetime2: False", new GregorianCalendar(2014, 10, 2)));

        /*
            Add appointment
         */
//        addApptmnt(appointments);

        /*
            Check date
         */
        GregorianCalendar testDate = new GregorianCalendar(2014, 9, 24);
        System.out.println("********************************");
        System.out.println("CHECKING ALL APPOINTS FOR TEST DATE");
        System.out.println("Test Date is: 9/24/2014");
        Occurs.valid(appointments, testDate);
        System.out.println("********************************");

        /*
            Save appointments
         */
        System.out.println("********************************");
        System.out.println("SAVING APPOINTMENT");
        Onetime apt1 = new Onetime("Car", new GregorianCalendar(2014, 10, 3));
        try {
                apt1.save();
        }
        catch (Exception exception) {
            System.out.println("Could not save file");
        }
        System.out.println("********************************");


        /*
            Load appointments
         */
        System.out.println("********************************");
        System.out.println("LOADING APPOINTMENT");
        Appointment.load();
        System.out.println("********************************");

    }

    /*
        Adds an appointment to the list of appointments
     */
    public static void addApptmnt(ArrayList<Occurs> arr) {
        String desc = "";
        String type = "";
        int year = -1;
        int month = -1;
        int day = -1;
        Scanner in = new Scanner(System.in);

        System.out.println("********************************");
        System.out.println("ADDING A NEW APPOINTMENT TO THE APPOINTMENT BOOK");

        //Get input for appointment description
        System.out.print("Enter a Description for your appointment: ");
        try {
            in.hasNext();
            desc = in.next();
        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No input for appointment description");
        }

        //Get input for appointment type
        System.out.print("Enter one of 'Onetime', 'Daily', 'Monthly' for your appointment type: ");
        try {
            type = (in.next()).toLowerCase();
            if (!type.equals("onetime") && !type.equals("daily") && !type.equals("monthly")) {
                type = "onetime";
                System.out.println("Warning: No type or invalid type inputted. Default type: 'Onetime'");
            }
        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No input for appointment type" );
        }

        //Get input for year
        try {
            System.out.print("Enter a year for your appointment: ");
            while (!in.hasNextInt()) {
                System.out.print("Please input an integer for year: ");
                in.next();
            }
            year = in.nextInt();
        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No input for year." );
        }

        //Get input for month
        try {
            while (true) {
                System.out.print("Enter a month for your appointment: ");
                while (!in.hasNextInt()) {
                    System.out.print("Please input an integer for month: ");
                    in.next();
                }
                month = in.nextInt();
                if (month > 0 && month < 13) {
                    break;
                }
                else {
                    System.out.print("Please enter a month between 1-12: ");
                }
            }

        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No input for month" );
        }


        //Get input for day of month
        try {
            while (true) {
                System.out.print("Enter a day for your appointment: ");
                while (!in.hasNextInt()) {
                    System.out.print("Please input an integer for day: ");
                    in.next();
                }
                day = in.nextInt();
                if (day > 0 && day < 32) {
                    break;
                }
                else {
                    System.out.print("Please enter a day between 1-31: ");
                }
            }




        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No input for day of month." );
        }


        //Only add an appointment if a valid month/day/year were entered
        if (year != -1 && month != -1 && day != -1) {
            if (type.equals("onetime")) {
                Onetime appt = new Onetime(desc, new GregorianCalendar(year, month, day));
                arr.add(appt);
                System.out.println(appt.toString() + " added!");
            } else if (type.equals("daily")) {
                Daily appt = new Daily(desc, new GregorianCalendar(year, month, day));
                arr.add(appt);
                System.out.println(appt.toString() + " added!");
            } else if (type.equals("monthly")) {
                Monthly appt = new Monthly(desc, new GregorianCalendar(year, month, day));
                arr.add(appt);
                System.out.println(appt.toString() + " added!");
            }
        }
        else {
            System.out.println("No Appointment added to the book");
        }
        System.out.println("********************************");

    }
}
