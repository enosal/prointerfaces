package P9_23;

import sun.util.calendar.Gregorian;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Appointment class
 * Methods to return appointment description, date (as a GregorianCalendar object), date (String object)
 * Created by Eryka on 10/25/2015.
 */
public abstract class Appointment implements Occurs {
    private String desc;
    private GregorianCalendar date;

    public Appointment(String desc, GregorianCalendar date) {
        this.desc = desc;
        this.date = date;
    }

    /*
        Return appointment description
     */
    public String getDesc() {
        return desc;
    }

    /*
        return a GregorianCalendar appointment date
     */
    public GregorianCalendar getDate() {
        return date;
    }

    /*
            return (M)M/(D)D/YYYY form of the appointment date (as a string)
    */
    public String dateString() {
        return date.get(Calendar.MONTH) + "/" + date.get(Calendar.DAY_OF_MONTH) + "/" + date.get(Calendar.YEAR);
    }

    /*
        Saves the date, description, and appointment type of an appointment as a "YYYY_MM_DD_appointmenttype_Appointment.txt" file
     */
    public void save() {
        try {
            //get Appointment type
            String className = this.getClass().getCanonicalName();
            String[] parts = className.split(Pattern.quote("."));
            String type = parts[1];

            //Open output file
            PrintWriter out = new PrintWriter(date.get(Calendar.YEAR) + "_" + date.get(Calendar.MONTH) + "_" + date.get(Calendar.DAY_OF_MONTH) + "_" + type + "_Appointment.txt");

            out.println(this.dateString());
            out.println(type);
            out.println(this.desc);

            //Close output file
            out.close();

            System.out.println("SUCCESS. Appointment saved.");
        } catch (IOException exception) {
            System.out.print("ERROR: Could not create output file.");
        }

    }

    /*
        Loads the date, description, and appointment type of an appointment from a "YYYY_MM_DD_appointmenttype_Appointment.txt" file
    */
    public static void load() {
        //Prompt for input file name
        Scanner console = new Scanner(System.in);
        System.out.print("Input file: ");
        String input_filename = "";
        try {
            input_filename = console.next();
        }
        catch (NoSuchElementException exception) {
            System.out.println("ERROR: No input file given");
        }

        try {
            //Open input file
            Scanner in = new Scanner(new File(input_filename));

            /*
            Read in lines
                Line1: MM/DD/YYYY
                Line2: Appointment type
                Line3: desc
             */
            String line1 = in.nextLine();
            String line2 = in.nextLine();
            String line3 = in.nextLine();

            //Open up a scanner on line 1
            Scanner line_scanner1 = new Scanner(line1);
            String[] parts = line1.split("/");

            //Get month, day, year for the date
            int month = Integer.parseInt(parts[0]);
            int day = Integer.parseInt(parts[1]);
            int year = Integer.parseInt(parts[2]);

            //Get desc and appointment type
            String type = line2;
            String desc = line3;

            //Create the proper object
            if (type.equals("Onetime")) {
                Onetime appt = new Onetime(desc, new GregorianCalendar(year, month, day));
                System.out.println(appt.toString() + " added!");
            } else if (type.equals("Daily")) {
                Daily appt = new Daily(desc, new GregorianCalendar(year, month, day));
                System.out.println(appt.toString() + " added!");
            } else if (type.equals("Monthly")) {
                Monthly appt = new Monthly(desc, new GregorianCalendar(year, month, day));
                System.out.println(appt.toString() + " added!");
            }


            //Close input file
            in.close();
            System.out.println("SUCCESS. Appointment loaded.");

        } catch (IOException exception) {
            System.out.println("ERROR: Could not open input file.");
        }

    }
}
