package P9_23;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Create a Monthly appointment from a given start date
 * Methods include whether an inputted date occurs on a monthly appointment date, and string description of monthly appointment
 * Created by Eryka on 10/25/2015.
 */
public class Monthly extends Appointment{

    private boolean occurs;

    public Monthly(String desc, GregorianCalendar date){
        super(desc, date);
    }

    /*
        Return true if the test date corresponds to a date on or one month after a monthly appointment start date
     */
    public boolean occursOn(int year, int month, int apptmentDay) {
        GregorianCalendar givenDate = new GregorianCalendar(year, month, apptmentDay);

        //Check if given date is the same as the appointment date
        if (givenDate.equals(super.getDate())) {
            occurs = true;
        }
        //Check if given date comes after the appointment date
        else if (givenDate.after(super.getDate())) {
            int givenDay = (super.getDate()).get(Calendar.DAY_OF_MONTH);
            //givenDay: inputted day
            //aptmentDay: appointment day

            //check if the given day is the same as the appointment day
            if (apptmentDay == givenDay) occurs = true;
            else occurs = false;
        }
        //For cases where the given date is before the appointment date
        else {
            occurs = false;
        }

        return occurs;
    }

    /*
        Returns appointment type (Monthly), start date of appointment, description of appointment
     */
    @Override
    public String toString() {
        return "Monthly Appointment starting on " + super.dateString() + ": '" + super.getDesc() + "'";
    }

}
