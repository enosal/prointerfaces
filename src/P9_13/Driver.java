package P9_13;

import java.awt.*;

/**
 * Tests LabeledPoint
 * Created by Eryka on 10/24/2015.
 */
public class Driver {
    public static void main(String[] args) {

        LabeledPoint point = new LabeledPoint(5, 7, "left");
        System.out.println(point.toString());
    }

}
