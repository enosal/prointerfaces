package P9_13;

import java.awt.*;

/**
 * LabeledPoint stores the location of the x, y coordinates in a Point object
 * LabeledPoint stores the label in a String
 *
 * Created by Eryka on 10/24/2015.
 */
public class LabeledPoint {

    /*
        Create instance variables: Point for the x, y location, String for the label
     */
    private Point point;
    private String label;

    /*
        Constructs Point object with x, y location and label
     */
    public LabeledPoint(int x, int y, String label) {
        point = new Point(x,y);
        this.label = label;
    }

    /*
       Returns info about the Location of the coordinates (by calling the Point object's toString method) and the Label of the coordinates
     */
    public String toString() {
        return "Location is in: " + point.toString() + ", Label is in: " + getClass().getName() + "[label='" + label + "']";
    }

}
