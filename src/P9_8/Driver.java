package P9_8;

/**
 * Tests superclass Person and subclasses Student and Instructor
 * Created by Eryka on 10/24/2015.
 */
public class Driver {
    public static void main(String[] args) {

        Student student1 = new Student("Eryka", "Nosal", 1989, "Computer Science");
        System.out.println(student1.toString());

        Instructor teacher1 = new Instructor("Mario", "Luigi", 1985, 123_456.3);
        System.out.println(teacher1.toString());

    }
}
