package P9_8;

/**
 * Create an object of Instructor. Return information about the instructor.
 * Created by Eryka on 10/24/2015.
 */
public class Instructor extends Person{

    /*
    Initialize instance variable salary specific to an Instructor
     */
    private double salary;

    /*
    Instructor Constructor sets salary as well as first name, last name, year of birth (from Person)
     */
    public Instructor(String fName, String lName, int year, double salary) {
        super(fName, lName, year);
        this.salary = salary;
    }

    /*
    Overridden toString method output information about an Instructor
     */
    @Override
    public String toString() {
        return super.getfName() + " " + super.getlName() + " is an Instructor born in " + getYear() + ", making $" + salary;
    }

}
