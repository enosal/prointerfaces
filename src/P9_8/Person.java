package P9_8;

/**
 * Superclass Person contains first and last name and year
 * An object of Person can't be created, but the methods to get year, first name, last name can be accessed by subclasses
 * Created by Eryka on 10/24/2015.
 */
public abstract class Person {
    /*
    Initialize instance variables for the Person class
     */
    private int year;
    private String fName;
    private String lName;

    /*
    Constructor with first name, last name, year of birth
     */

    public Person(String fName, String lName, int year) {
        this.year = year;
        this.fName = fName;
        this.lName = lName;
    }

    /*
    Returns a Person's year (of birth)
     */

    public int getYear() {
        return year;
    }

    /*
    Returns a Person's first name
     */
    public String getfName() {
        return fName;
    }

    /*
    Returns a Person's last name
     */
    public String getlName() {
        return lName;
    }
}
