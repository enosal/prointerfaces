package P9_8;

/**
 * Create an object of Student. Return information about the student.
 * Created by Eryka on 10/24/2015.
 */
public class Student extends Person {

    /*
    Initialize instance variable major specific to a Student
    */
    private String major;

    /*
    Student constructor sets major as well as first name, last name, year of birth (from Person)
     */
    public Student(String fName, String lName, int year, String major) {
        super(fName, lName, year);
        this.major = major;
    }

    /*
    Overridden toString method outputs information about a Student
     */
    @Override
    public String toString() {
        return super.getfName() + " " + super.getlName() + " is a Student born in " + getYear() + ", majoring in " + major;
    }
}
