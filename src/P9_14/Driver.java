package P9_14;


/**
 * Tests Measurable interface on an array of Measurable SodaCans
 * Created by Eryka on 10/24/2015.
 */
public class Driver {
    public static void main(String[] args) {
        Measurable[] cans = new Measurable[3];
        cans[0] = new SodaCan(3,4);
        cans[1] = new SodaCan(1,5);
        cans[2] = new SodaCan(2,2);

        SodaCan can0 = (SodaCan) cans[0];
        SodaCan can1 = (SodaCan) cans[1];
        SodaCan can2 = (SodaCan) cans[2];
        System.out.println("TEST1: 3 Soda cans");
        System.out.printf("Expected Average Surface Area of soda cans: %.2f%n", (can0.getSurfaceArea() + can1.getSurfaceArea() + can2.getSurfaceArea()) / 3.0);
        System.out.printf("Average surface area of soda cans:  %.2f%n", Measurable.average(cans));
        System.out.println();


        Measurable[] cans1 = new Measurable[0];
        System.out.println("TEST2: 0 Soda cans");
        System.out.println("Expected Average Surface Area of soda cans: 0.00");
        System.out.printf("Average surface area of soda cans:  %.2f%n", Measurable.average(cans1));


    }
}
