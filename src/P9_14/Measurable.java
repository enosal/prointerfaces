package P9_14;

/**
 * Interface for Measurable objects
 * Created by Eryka on 10/24/2015.
 */
public interface Measurable {
    double getMeasure();


    /*
        Takes the average of objects inside a Measureable array
     */
    static double average(Measurable[] objects) {
        double sum = 0;
        double avg;


        /*
            Get total getMeasure
         */
        for (Measurable object : objects) {
            sum += object.getMeasure();
        }


        if (objects.length == 0) {
            avg = 0;
        }
        else {
            avg = sum/objects.length;
        }

        return avg;
    }

}
